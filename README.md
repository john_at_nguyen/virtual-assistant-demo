# Virtual Assistant Demo #

Hi there! This repository acts as a guide for making your own personal voice assistant. Each step of the process and each technology used has its own branch so you can learn one step at a time! Well, that's it - I hope you enjoy! 

-John

## Guide ##

1. [Creating a Voice-Interactive Front-End]()

2. [Setting up the NodeJS Server]()

3. [Using API.AI for Natural Language Processing]()

4. [Wrapping It Up and Deploying It Out]()

### Technologies ###

* [HTML]()

* [CSS]()

* [Javascript]()

* [AngularJS]()

* [Firebase]()

* [Google Web Speech API]()

* [API.AI]()

* [ResponsiveVoice.JS]()

* [NodeJS]()

* [ExpressJS]()

### Contact Information ###

* E-mail me at [crystalfruit.john@gmail.com](mailto:crystalfruit.john@gmail.com) if you have any questions!